package com.shevtsovaa.domain;

import java.util.ArrayList;

public class Accountants extends Employee {
    public static ArrayList<Accountants> accountants;
    public String surName;
    public String givenName;
    public String login;
    public String password;

    public Accountants(String surName, String givenName, String login, String password,String homeAddress, String personalPhoneNumber) {
        super(homeAddress, personalPhoneNumber);
        this.surName = surName;
        this.givenName = givenName;
        this.login = login;
        this.password = password;

    }


    @Override
    public String toString() {
        return "Accountants{" +
                "surName='" + surName + '\'' +
                ", givenName='" + givenName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Accountants acc1 = new Accountants("Elizabeth", "Smith", "elismi", "11111",
                "address1",  "333-333-333");
        accountants.add(acc1);
        Accountants acc2 = new Accountants("Ann", "Rouse", "gunsandroses", "22222", "address2", "555-555-555");
        accountants.add(acc2);
    }



}
