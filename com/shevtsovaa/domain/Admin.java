package com.shevtsovaa.domain;

import java.util.HashMap;
import java.util.Map;

public class Admin extends Employee {
    private String surName;
    private String givenName;

    public Admin(String homeAddress, String personalPhoneNumber, String surName, String givenName) {
        super(homeAddress, personalPhoneNumber);
        this.surName = surName;
        this.givenName = givenName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "surName='" + surName + '\'' +
                ", givenName='" + givenName + '\'' +
                '}';
    }

    public static Map<Accountants, Boolean> accountantsWithValidationStatus = new HashMap<>();

    public static HashMap<Accountants, Boolean> reloadAccountsData() {


        for (Accountants s : Accountants.accountants) {
            accountantsWithValidationStatus.put(s, setStatus());
        }
        return new HashMap<>();
    }
    public static Boolean setStatus() {
         UnsupportedOperationException ex;
        return null; //Not implemented yet.
    }

}