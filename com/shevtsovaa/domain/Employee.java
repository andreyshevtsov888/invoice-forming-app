package com.shevtsovaa.domain;

public class Employee {

    private String homeAddress;
    private String personalPhoneNumber;

    public Employee(String homeAddress, String personalPhoneNumber) {
        this.homeAddress = homeAddress;
        this.personalPhoneNumber = personalPhoneNumber;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getPersonalPhoneNumber() {
        return personalPhoneNumber;
    }

    public void setPersonalPhoneNumber(String personalPhoneNumber) {
        this.personalPhoneNumber = personalPhoneNumber;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "homeAddress='" + homeAddress + '\'' +
                ", personalPhoneNumber='" + personalPhoneNumber + '\'' +
                '}';
    }
}
