package com.shevtsovaa.domain;

public class Counterparty {
    String country;
    String city;
    String street;
    String building;
    String phoneNumber;
    String contactPersonName;

    public Counterparty(String country, String city, String street, String building, String phoneNumber, String contactPersonName) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.building = building;
        this.phoneNumber = phoneNumber;
        this.contactPersonName = contactPersonName;
    }


    @Override
    public String toString() {
        return "Requisites{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", building='" + building + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", contactPersonName='" + contactPersonName + '\'' +
                '}';
    }
}
