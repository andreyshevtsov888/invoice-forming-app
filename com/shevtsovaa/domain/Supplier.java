package com.shevtsovaa.domain;

public class Supplier extends Counterparty {
    String supplierName;
    public Supplier(String country, String city, String street, String building, String phoneNumber, String contactPersonName, String supplierName) {
        super(country, city, street, building, phoneNumber, contactPersonName);
        this.supplierName = supplierName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "supplierName='" + supplierName + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", building='" + building + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", contactPersonName='" + contactPersonName + '\'' +
                '}';
    }
}
