package com.shevtsovaa.domain;

public enum GoodUnits {
    Kg ("kilogram"),
    Pcs ("pieces"),
    Lit ("liters"),
    M ("metres");

    private final String title;

    GoodUnits(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "GoodUnits{" +
                "title='" + title + '\'' +
                '}';
    }
}
