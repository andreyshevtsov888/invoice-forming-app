package com.shevtsovaa;

import com.shevtsovaa.display.DisplayToConsole;
import com.shevtsovaa.display.DisplayToPdfFile;
import com.shevtsovaa.display.Displayable;
import com.shevtsovaa.validationandinput.AccessValidator;
import java.io.IOException;
import static com.shevtsovaa.validationandinput.ConsoleDataInput.*;

public class InvoiceApp {

    public static void main(String[] args) {

        try {
            validationInfoInput();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        boolean access = AccessValidator.validate();
        if (access) {
            System.out.println("You have no access!");

        } else System.out.println(" "); // Not implemented yet.

        try {
            invoiceDataInput();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            goodsDataInput();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Displayable displayToConsole = new DisplayToConsole();
        displayToConsole.displayInvoice();

        Displayable displayToPDF = new DisplayToPdfFile();
        displayToPDF.displayInvoice();
    }

}
