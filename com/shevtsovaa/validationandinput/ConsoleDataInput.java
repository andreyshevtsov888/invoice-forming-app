package com.shevtsovaa.validationandinput;

import com.shevtsovaa.domain.GoodUnits;
import com.shevtsovaa.invoice.GoodsInInvoice;
import com.shevtsovaa.invoice.InvoiceData;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ConsoleDataInput {

    public static String[] validationSet = new String[2];
    public static InvoiceData invoiceD;
    public static List<GoodsInInvoice> goodsInInvoice = new ArrayList<>();


    public static void validationInfoInput() throws IOException {
        String login;
        String password;
        Scanner sc = new Scanner(System.in);
        System.out.println("Input login:");
        login = sc.nextLine();
        validationSet[0] = login;
        System.out.println("Input password:");
        password = sc.nextLine();
        System.out.println(login + " " + password);
        validationSet[1] = password;
        System.out.println(Arrays.toString(validationSet));

    }


    public static void invoiceDataInput() throws IOException {

        Scanner sc1 = new Scanner(System.in);
        LocalDate date;
        String number;
        int vat;

        System.out.println("Input invoice date:");
        date = LocalDate.parse(sc1.nextLine());
        System.out.println("Input invoice number:");
        number = sc1.nextLine();
        System.out.println("Choose supplier from this list end enter or enter new one if it isn't in list yet:");
        // Not implemented yet.
        System.out.println("Input invoice VAT in percents:");
        vat = sc1.nextInt();
        invoiceD = new InvoiceData(date, number, null, vat, invoiceSum());
    }

    public static void goodsDataInput() throws IOException {
        Scanner sc2 = new Scanner(System.in);
        String goodsName;
        GoodUnits gu;
        double goodsQuantity;
        double pricePerUnit;

        do {
            System.out.println("Input goods name:");
            goodsName = sc2.nextLine();
            System.out.println("Input units:");
            gu = GoodUnits.valueOf(sc2.nextLine());
            System.out.println("Input quantity of goods:");
            goodsQuantity = sc2.nextDouble();
            System.out.println("Input price per unit:");
            pricePerUnit = sc2.nextDouble();
            GoodsInInvoice gii = new GoodsInInvoice(goodsName, gu, goodsQuantity, pricePerUnit, goodsQuantity*pricePerUnit);
            goodsInInvoice.add(gii);
            System.out.println("Press enter for next line or q for quit:");
        }
        while (sc2.nextLine().equals("q"));
    }
    public static double invoiceSum() {
        UnsupportedOperationException ex; //Not implemented yet.
        return 0;
    }




}


