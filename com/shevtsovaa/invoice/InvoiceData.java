package com.shevtsovaa.invoice;

import com.shevtsovaa.domain.Supplier;
import java.time.LocalDate;

public class InvoiceData {

    public LocalDate invoiceDate;
    public String invoiceNumber;
    public Supplier supplier;
    public int vatRate;
    public double sumTotal;

    public InvoiceData(LocalDate invoiceDate, String invoiceNumber, Supplier supplier, int vatRate, double sumTotal) {
        this.invoiceDate = invoiceDate;
        this.invoiceNumber = invoiceNumber;
        this.supplier = supplier;
        this.vatRate = vatRate;
        this.sumTotal = sumTotal;
    }


}
