package com.shevtsovaa.invoice;

import com.shevtsovaa.domain.GoodUnits;
public class GoodsInInvoice {
    public String goodsName;
    public GoodUnits gu;
    public double goodsQuantity;
    public double pricePerUnit;
    public double sumPerLine;

    public GoodsInInvoice(String goodsName, GoodUnits gu, double goodsQuantity, double pricePerUnit, double sumPerLine) {
        this.goodsName = goodsName;
        this.gu = gu;
        this.goodsQuantity = goodsQuantity;
        this.pricePerUnit = pricePerUnit;
        this.sumPerLine = goodsQuantity*pricePerUnit;
    }

}
